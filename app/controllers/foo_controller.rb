class FooController < ApplicationController
  def index
    puts "index"
  end

  def show
    puts "show"
    idPedido = params[:id]
    ipDaRequisicao = request.remote_ip
    render json: {idPedido: idPedido, seuIp: ipDaRequisicao}, status: :ok
  end

  def new
    puts "new"
  end

  def create
    puts "create"
    receivedObject = JSON.parse(request.body.read)
    puts "json recebido"
    puts receivedObject
    status = receivedObject["status"]
    message = receivedObject["message"]
    render json: {status: status, message: message, myParam: params, myBody: receivedObject}, status: :ok
  end

  def edit
    puts "edit"
  end

  def update
    puts "update"
  end

  def destroy
    puts "destroy"
  end
end

require 'test_helper'

class FooControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get foo_index_url
    assert_response :success
  end

  test "should get show" do
    get foo_show_url
    assert_response :success
  end

  test "should get new" do
    get foo_new_url
    assert_response :success
  end

  test "should get create" do
    get foo_create_url
    assert_response :success
  end

  test "should get edit" do
    get foo_edit_url
    assert_response :success
  end

  test "should get update" do
    get foo_update_url
    assert_response :success
  end

  test "should get destroy" do
    get foo_destroy_url
    assert_response :success
  end

end
